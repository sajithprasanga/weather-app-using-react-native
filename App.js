/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {View, StyleSheet, Alert, TouchableWithoutFeedback, Keyboard, ImageBackground } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';


import {getWeather} from './store/actions/weatherActions';
import Form from './components/Form';
import Weather from './components/Weather';


const App = () => {
  const [search, setSearch ] = useState('');
  const [loading, setLoading ] = useState(false);
  const dispatch = useDispatch();
  const {data, error } = useSelector(state => state.weather);

  const searchSubitHandler = () => {
    if (search === '') {
      return Alert.alert('Validation', 'city name is required!', [{text: 'OK' }]);
    }

    setLoading(true);
    dispatch(getWeather(search, () => setLoading(false), () => setLoading(false)));
    setSearch('');
    Keyboard.dismiss();
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <Form search={search} onSetSearch={setSearch} onSubmit={searchSubitHandler} />
  
        <Weather loading={loading} data={data} error={error} />
     
        
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container:{
    flex: 1,
  },

  con1: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
